<main class="col-md markdown-body" style="margin-bottom: 1017px;"><a data-id="" id="capstone"></a>

# [Capstone](#capstone)

Designing and implementing a web application of your own with Python and JavaScript.

<a data-id="" id="overview"></a>

## [Overview](#overview)

The final project is your opportunity to design and implement a dynamic website of your own. So long as your final project draws upon this course’s lessons, the nature of your website will be entirely up to you.

<a data-id="" id="requirements"></a>

## [Requirements](#requirements)

In this project, you are asked to build a web application of your own. The nature of the application is up to you, subject to a few requirements:

*   <span class="fa-li"></span>Your web application must be sufficiently distinct from the other projects in this course (and, in addition, may not be based on the [old CS50W Pizza project](https://docs.cs50.net/web/2020/x/projects/3/project3.html)), and more complex than those.
*   <span class="fa-li"></span>Your web application must utilize Django (including at least one model) on the back-end and JavaScript on the front-end.
*   <span class="fa-li"></span>Your web application must be mobile-responsive.
*   <span class="fa-li"></span>In a `README.md` in your project’s main directory, include a short writeup describing your project, what’s contained in each file you created, and (optionally) any other additional information the staff should know about your project. This file should also provide your justification for why you believe your project satisfies the distinctiveness and complexity requirements, mentioned above.
*   <span class="fa-li"></span>If you’ve added any Python packages that need to be installed in order to run your web application, be sure to add them to a `requirements.txt` file!

Beyond these requirements, the design, look, and feel of the website are up to you!

<a data-id="" id="how-to-submit"></a>

## [How to Submit](#how-to-submit)

<div class="alert alert-danger" data-alert="danger" role="alert">

**Hold on!** If you have already submitted and received a passing grade on the [prior version of the Final Project](https://docs.cs50.net/web/2020/x/projects/final/final.html), please stop here. You already have received an equivalence credit for this project, and you should not submit this assignment, as it will have no impact on your progress in the course and will therefore only slow our graders down!

</div>

1.  Visit [this link](https://submit.cs50.io/invites/89679428401548238ceb022f141b9947), log in with your GitHub account, and click **Authorize cs50**. Then, check the box indicating that you’d like to grant course staff access to your submissions, and click **Join course**.
2.  [Install Git](https://git-scm.com/downloads) and, optionally, [install `submit50`](https://cs50.readthedocs.io/submit50/).

<div class="alert alert-success" data-alert="success" role="alert">

When you submit your project, the structure of your `web50/projects/2020/x/capstone` branch should match, in general, the file structure of Projects 1, 2, 3, and 4\. Your branch should also not contain any code from any other projects, only this one. Failure to adhere to this file structure will likely result in your submission being rejected.

Your `README.md` file must also be at the highest level of your project. That is to say it should exist at `https://github.com/me50/USERNAME/blob/web50/projects/2020/x/capstone/README.md` (where `USERNAME` is your own GitHub username as provided in the form, below). If it doesn’t, reorganize your repository as needed to match this paradigm.

</div>

1.  If you’ve installed `submit50`, execute

    <div class="highlighter-rouge">

    <div class="highlight">

        submit50 web50/projects/2020/x/capstone

    </div>

    </div>

    Otherwise, using Git, push your work to `https://github.com/me50/USERNAME.git`, where `USERNAME` is your GitHub username, on a branch called `web50/projects/2020/x/capstone`.

2.  [Record a screencast](https://www.howtogeek.com/205742/how-to-record-your-windows-mac-linux-android-or-ios-screen/) not to exceed 5 minutes in length, in which you demonstrate your project’s functionality. Be certain that every requirement of the specification, above, is demonstrated in your video. There’s no need to show your code in this video, just your application in action; we’ll review your code on GitHub. [Upload that video to YouTube](https://www.youtube.com/upload) (as unlisted or public, but not private) or somewhere else.
3.  Submit [this form](https://forms.cs50.io/8c32745a-dab0-423d-ada4-877bcd3c395c).

You can then go to [https://cs50.me/cs50w](https://cs50.me/cs50w) to view your current progress!

</main>