## Hello, World

## harvard.edu
my study flow

# About CS50w

Brian Yu

David J. Malan

## CS50’s Web Programming with Python and JavaScript

This course picks up where Harvard University’s CS50 leaves off, diving more deeply into the design and implementation of web apps with Python, JavaScript, and SQL using frameworks like Django, React, and Bootstrap. Topics include database design, scalability, security, and user experience. Through hands-on projects, students learn to write and use APIs, create interactive UIs, and leverage cloud services like GitHub and Heroku. By semester’s end, students emerge with knowledge and experience in principles, languages, and tools that empower them to design and deploy applications on the Internet.



Prerequisites: CS50x or prior experience in any programming language.
This site is for the version of CS50W that debuted 1 July 2020. If you’ve prefer to view the material for the prior version of the course, you still may do so, though note it is no longer possible to submit that version’s projects.

<img src="CS50X_inro/img/elements.png" alt="drawing" style="width:400px"/>


## Course Topics: 
### We’ll go into more detail later, but here’s a brief overview of what we’ll be working on during this course:
### Weeks

## 0. HTML and CSS (a markup language used to outline a webpage, and a procedure for making our sites more visually appealing)

*   [Introduction](CS50W_Week0/readme.md#introduction)
*   [Web Programming](CS50W_Week0/readme.md#web-programming)
*   [HTML (Hypertext Markup Language)](CS50W_Week0/readme.md#html-hypertext-markup-language)
    *   [Document Object Model (DOM)](CS50W_Week0/readme.md#document-object-model-dom)
    *   [More HTML Elements](CS50W_Week0/readme.md#more-html-elements)
    *   [Forms](CS50W_Week0/readme.md#forms)
*   [CSS (Cascading Style Sheets)](CS50W_Week0/readme.md#css-cascading-style-sheets)
*   [Responsive Design](CS50W_Week0/readme.md#responsive-design)
*   [Bootstrap](CS50W_Week0/readme.md#bootstrap)
*   [Sass (Syntactically Awesome Style Sheets)](CS50W_Week0/readme.md#sass-syntactically-awesome-style-sheets)

## 1. Git (used for version control and collaboration)
*   [Introduction](CS50W_Week1/readme.md#introduction)
*   [Git](CS50W_Week1/readme.md#git)
*   [GitHub](CS50W_Week1/readme.md#github)
*   [Commits](CS50W_Week1/readme.md#commits)
*   [Merge Conflicts](CS50W_Week1/readme.md#merge-conflicts)
*   [Branching](CS50W_Week1/readme.md#branching)
    *   [More GitHub Features](CS50W_Week1/readme.md#more-github-features)

## 2. Python (a widely-used programming language we’ll use to make our sites more dynamic), Flask
*   [Introduction](CS50W_Week2/readme.md#introduction)
*   [Python](CS50W_Week2/readme.md#python)
*   [Variables](CS50W_Week2/readme.md#variables)
*   [Formatting Strings](CS50W_Week2/readme.md#formatting-strings)
*   [Conditions](CS50W_Week2/readme.md#conditions)
*   [Sequences](CS50W_Week2/readme.md#sequences)
    *   [Strings](CS50W_Week2/readme.md#strings)
    *   [Lists](CS50W_Week2/readme.md#lists)
    *   [Tuples](CS50W_Week2/readme.md#tuples)
    *   [Sets](CS50W_Week2/readme.md#sets)
    *   [Dictionaries](CS50W_Week2/readme.md#dictionaries)
    *   [Loops](CS50W_Week2/readme.md#loops)
*   [Functions](CS50W_Week2/readme.md#functions)
*   [Modules](CS50W_Week2/readme.md#modules)
*   [Object-Oriented Programming](CS50W_Week2/readme.md#object-oriented-programming)
*   [Functional Programming](CS50W_Week2/readme.md#functional-programming)
    *   [Decorators](CS50W_Week2/readme.md#decorators)
    *   [Lambda Functions](CS50W_Week2/readme.md#lambda-functions)
*   [Exceptions](CS50W_Week2/readme.md#exceptions)

## 3. Django (a popular web framework we’ll use for the backend of our sites)
*   [Introduction](CS50W_Week3/readme.md#introduction)
*   [Web Applications](CS50W_Week3/readme.md#web-applications)
*   [HTTP](CS50W_Week3/readme.md#http)
*   [Django](CS50W_Week3/readme.md#django)
*   [Routes](CS50W_Week3/readme.md#routes)
*   [Templates](CS50W_Week3/readme.md#templates)
    *   [Conditionals:](CS50W_Week3/readme.md#conditionals)
    *   [Styling](CS50W_Week3/readme.md#styling)
*   [Tasks](CS50W_Week3/readme.md#tasks)
*   [Forms](CS50W_Week3/readme.md#forms)
    *   [Django Forms](CS50W_Week3/readme.md#django-forms)
*   [Sessions](CS50W_Week3/readme.md#sessions)

## 4. SQL, Models, and Migrations (a language used for storing and retrieving data, and Django-specific methods that make it easier to interact with SQL databases), ORMs, APIs
*   [Introduction](CS50W_Week4/readme.md#introduction)
*   [SQL](CS50W_Week4/readme.md#sql)
    *   [Databases](CS50W_Week4/readme.md#databases)
    *   [Column Types](CS50W_Week4/readme.md#column-types)
*   [Tables](CS50W_Week4/readme.md#tables)
*   [SELECT](CS50W_Week4/readme.md#select)
    *   [Working with SQL in the Terminal](CS50W_Week4/readme.md#working-with-sql-in-the-terminal)
    *   [Functions](CS50W_Week4/readme.md#functions)
    *   [UPDATE](CS50W_Week4/readme.md#update)
    *   [DELETE](CS50W_Week4/readme.md#delete)
    *   [Other Clauses](CS50W_Week4/readme.md#other-clauses)
*   [Joining Tables](CS50W_Week4/readme.md#joining-tables)
    *   [JOIN Query](CS50W_Week4/readme.md#join-query)
    *   [Indexing:](CS50W_Week4/readme.md#indexing)
    *   [SQL Vulnerabilities](CS50W_Week4/readme.md#sql-vulnerabilities)
*   [Django Models](CS50W_Week4/readme.md#django-models)
*   [Migrations](CS50W_Week4/readme.md#migrations)
*   [Shell](CS50W_Week4/readme.md#shell)
    *   [Starting our application](CS50W_Week4/readme.md#starting-our-application)
*   [Django Admin](CS50W_Week4/readme.md#django-admin)
*   [Many-to-Many Relationships](CS50W_Week4/readme.md#many-to-many-relationships)
*   [Users](CS50W_Week4/readme.md#users)

## 5. JavaScript (a programming language used to make websites faster and more interactive)

## 6. Front Ends, User Interfaces (methods used to make a website as easy to use as possible)

## 7. Testing, CI, CD (learning about different methods used to make sure updates to web pages proceed smoothly), GitHub, Travis CI

## 8. Scalability and Security (making sure our websites can be accessed by many users at once, and that they are safe from malicious intent)




## Be sure to submit the Final Project anytime before 31 December 2020.

* * *
### About CS50x

David J. Malan

Introduction to the intellectual enterprises of computer science and the art of programming. This course teaches students how to think algorithmically and solve problems efficiently. Topics include abstraction, algorithms, data structures, encapsulation, resource management, security, and software engineering. Languages include C, Python, and SQL plus students’ choice of: HTML, CSS, and JavaScript (for web development); Java or Swift (for mobile app development); or Lua (for game development). Problem sets inspired by the arts, humanities, social sciences, and sciences. Course culminates in a final project. Designed for concentrators and non-concentrators alike, with or without prior programming experience. Two thirds of CS50 students have never taken CS before. Among the overarching goals of this course are to inspire students to explore unfamiliar waters, without fear of failure, create an intensive, shared experience, accessible to all students, and build community among students.


What you will learn

- A broad and robust understanding of computer science, programming, and software development
- Concepts like abstraction, algorithms, data structures, encapsulation, resource management, security, software engineering, and web development
- Familiarity in a number of languages, including C, Python, JavaScript, SQL, CSS, and HTML
-Principles of 2D graphics,

* * *

## CS50's Courses

* CS50 x
  > [This is CS50x](https://cs50.harvard.edu/x/2020/)

  > [CS50x – Discussion](https://us.edstem.org/courses/176/discussion/78334)
* CS50 AI
* CS50 Business
* CS50 Games
* CS50 Law
* CS50 Technology
*  **CS50 Web**  
   > [CS50’s Web Programming with Python and JavaScript](https://cs50.harvard.edu/web/2020/) 

   > [This is CS50W 2018, an older version of the course.](https://cs50.harvard.edu/web/2018/)

* CS50 Mobile

* * *

[<img src="CS50X_inro/img/uc_m.png" alt="drawing" style="width:200px; background-color: gray; padding: 20px"/>](https://cs50.harvardshop.com/)

* * *

- CC [License](https://cs50.harvard.edu/web/2020/license/)
- CC [BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
- NonCommercial 
