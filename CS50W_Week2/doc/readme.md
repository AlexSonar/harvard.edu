<h1>Commerce</h1>

<p>Design an eBay-like e-commerce auction site that will allow users to 
post auction listings, place bids on listings, comment on those 
listings, and add listings to a “watchlist.”</p>

<p><img src="Commerce_files/listings.png" alt="Active listings page"></p>

<p><img src="Commerce_files/listing.png" alt="Listing page"></p>

<a data-id="" id="getting-started"></a><h2><a data-id="" href="#getting-started">Getting Started</a></h2>

[commerce](https://cs50.harvard.edu/web/2020/projects/2/commerce/)

<ol>
  <li>Download the distribution code from <a href="https://cdn.cs50.net/web/2020/spring/projects/2/commerce.zip">https://cdn.cs50.net/web/2020/spring/projects/2/commerce.zip</a> and unzip it.</li>
  <li>In your terminal, <code class="highlighter-rouge">cd</code> into the <code class="highlighter-rouge">commerce</code> directory.</li>
  <li>Run <code class="highlighter-rouge">python manage.py makemigrations auctions</code> to make migrations for the <code class="highlighter-rouge">auctions</code> app.</li>
  <li>Run <code class="highlighter-rouge">python manage.py migrate</code> to apply migrations to your database.</li>
</ol>

<a data-id="" id="understanding"></a><h2><a data-id="" href="#understanding">Understanding</a></h2>

<p>In the distribution code is a Django project called <code class="highlighter-rouge">commerce</code> that contains a single app called <code class="highlighter-rouge">auctions</code>.</p>

<p>First, open up <code class="highlighter-rouge">auctions/urls.py</code>,
 where the URL configuration for this app is defined. Notice that we’ve 
already written a few URLs for you, including a default index route, a <code class="highlighter-rouge">/login</code> route, a <code class="highlighter-rouge">/logout</code> route, and a <code class="highlighter-rouge">/register</code> route.</p>

<p>Take a look at <code class="highlighter-rouge">auctions/views.py</code> to see the views that are associated with each of these routes. The index view for now returns a mostly-empty <code class="highlighter-rouge">index.html</code> template. The <code class="highlighter-rouge">login_view</code>
 view renders a login form when a user tries to GET the page. When a 
user submits the form using the POST request method, the user is 
authenticated, logged in, and redirected to the index page. The <code class="highlighter-rouge">logout_view</code> view logs the user out and redirects them to the index page. Finally, the <code class="highlighter-rouge">register</code>
 route displays a registration form to the user, and creates a new user 
when the form is submitted. All of this is done for you in the 
distribution code, so you should be able to run the application now to 
create some users.</p>

<p>Run <code class="highlighter-rouge">python manage.py runserver</code>
 to start up the Django web server, and visit the website in your 
browser. Click “Register” and register for an account. You should see 
that you are now “Signed in as” your user account, and the links at the 
top of the page have changed. How did the HTML change? Take a look at <code class="highlighter-rouge">auctions/templates/auctions/layout.html</code> for the HTML layout of this application. Notice that several parts of the template are wrapped in a check for if <code class="highlighter-rouge">user.is_authenticated</code>,
 so that different content can be rendered depending on whether the user
 is signed in or not. You’re welcome to change this file if you’d like 
to add or modify anything in the layout!</p>

<p>Finally, take a look at <code class="highlighter-rouge">auctions/models.py</code>.
 This is where you will define any models for your web application, 
where each model represents some type of data you want to store in your 
database. We’ve started you with a <code class="highlighter-rouge">User</code> model that represents each user of the application. Because it inherits from <code class="highlighter-rouge">AbstractUser</code>, it will already have fields for a username, email, password, etc., but you’re welcome to add new fields to the <code class="highlighter-rouge">User</code>
 class if there is additional information about a user that you wish to 
represent. You will also need to add additional models to this file to 
represent details about auction listings, bids, comments, and auction 
categories. Remember that each time you change anything in <code class="highlighter-rouge">auctions/models.py</code>, you’ll need to first run <code class="highlighter-rouge">python manage.py makemigrations</code> and then <code class="highlighter-rouge">python manage.py migrate</code> to migrate those changes to your database.</p>

<a data-id="" id="specification"></a><h2><a data-id="" href="#specification">Specification</a></h2>

<p>Complete the implementation of your auction site. You must fulfill the following requirements:</p>

<ul class="fa-ul">
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span><strong>Models</strong>: Your application should have at least three models in addition to the <code class="highlighter-rouge">User</code>
 model: one for auction listings, one for bids, and one for comments 
made on auction listings. It’s up to you to decide what fields each 
model should have, and what the types of those fields should be. You may
 have additional models if you would like.</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span><strong>Create Listing</strong>:
 Users should be able to visit a page to create a new listing. They 
should be able to specify a title for the listing, a text-based 
description, and what the starting bid should be. Users should also 
optionally be able to provide a URL for an image for the listing and/or a
 category (e.g. Fashion, Toys, Electronics, Home, etc.).</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span><strong>Active Listings Page</strong>:
 The default route of your web application should let users view all of 
the currently active auction listings. For each active listing, this 
page should display (at minimum) the title, description, current price, 
and photo (if one exists for the listing).</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span><strong>Listing Page</strong>:
 Clicking on a listing should take users to a page specific to that 
listing. On that page, users should be able to view all details about 
the listing, including the current price for the listing.
    <ul class="fa-ul">
      <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>If
 the user is signed in, the user should be able to add the item to their
 “Watchlist.” If the item is already on the watchlist, the user should 
be able to remove it.</li>
      <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>If
 the user is signed in, the user should be able to bid on the item. The 
bid must be at least as large as the starting bid, and must be greater 
than any other bids that have been placed (if any). If the bid doesn’t 
meet those criteria, the user should be presented with an error.</li>
      <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>If
 the user is signed in and is the one who created the listing, the user 
should have the ability to “close” the auction from this page, which 
makes the highest bidder the winner of the auction and makes the listing
 no longer active.</li>
      <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>If a user is signed in on a closed listing page, and the user has won that auction, the page should say so.</li>
      <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>Users
 who are signed in should be able to add comments to the listing page. 
The listing page should display all comments that have been made on the 
listing.</li>
    </ul>
  </li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span><strong>Watchlist</strong>:
 Users who are signed in should be able to visit a Watchlist page, which
 should display all of the listings that a user has added to their 
watchlist. Clicking on any of those listings should take the user to 
that listing’s page.</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span><strong>Categories</strong>:
 Users should be able to visit a page that displays a list of all 
listing categories. Clicking on the name of any category should take the
 user to a page that displays all of the active listings in that 
category.</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span><strong>Django Admin Interface</strong>:
 Via the Django admin interface, a site administrator should be able to 
view, add, edit, and delete any listings, comments, and bids made on the
 site.</li>
</ul>

<a data-id="" id="hints"></a><h2><a data-id="" href="#hints">Hints</a></h2>

<ul class="fa-ul">
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>To create a superuser account that can access Django’s admin interface</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>See Django’s <a href="https://docs.djangoproject.com/en/3.0/ref/models/fields/">Model field reference</a> for possible field types for your Django model.</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>You’ll likely need to create some <a href="https://docs.djangoproject.com/en/3.0/topics/forms/">Django forms</a> for various parts of this web application.</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>Adding the <a href="https://docs.djangoproject.com/en/3.0/topics/auth/default/#the-login-required-decorator"><code class="highlighter-rouge">@login_required</code> decorator</a> on top of any view will ensure that only a user who is logged in can access that view.</li>
  <li data-marker="*"><span class="fa-li"><i class="fas fa-circle"></i></span>You’re
 welcome to modify the CSS as much as you’d like, to make the website 
your own! Some sample screenshots are shown at the top of this page. 
These are meant only to be examples: your application need not be 
aesthetically the same as the screenshots here (you’re encouraged to be 
creative!).</li>
</ul>

<a data-id="" id="how-to-submit"></a><h2><a data-id="" href="#how-to-submit">How to Submit</a></h2>

<div class="alert alert-danger" data-alert="danger" role="alert">
  <p><strong>Hold on!</strong> If you have already submitted and received a passing grade on the <a href="https://docs.cs50.net/web/2020/x/projects/1/project1.html" class="alert-link">prior version of Project 1</a>
 (Books), please stop here. You already have received an equivalence 
credit for this project, and you should not submit this assignment, as 
it will have no impact on your progress in the course and will therefore
 only slow our graders down!</p>
</div>

<ol>
  <li>Visit <a href="https://submit.cs50.io/invites/89679428401548238ceb022f141b9947">this link</a>, log in with your GitHub account, and click <strong>Authorize cs50</strong>. Then, check the box indicating that you’d like to grant course staff access to your submissions, and click <strong>Join course</strong>.</li>
  <li><a href="https://git-scm.com/downloads">Install Git</a> and, optionally, <a href="https://cs50.readthedocs.io/submit50/">install <code class="highlighter-rouge">submit50</code></a>.</li>
</ol>

<div class="alert alert-success" data-alert="success" role="alert">
  <p>When you submit your project, the contents of your <code class="highlighter-rouge">web50/projects/2020/x/commerce</code>
 branch should match the file structure of the unzipped distribution 
code as originally received. That is to say, your files should not be 
nested inside of any other directories of your own creation. Your branch
 should also not contain any code from any other projects, only this 
one. Failure to adhere to this file structure will likely result in your
 submission being rejected.</p>

<p>By way of example, for this project that means that if the grading staff visits <code class="highlighter-rouge">https://github.com/me50/USERNAME/tree/web50/projects/2020/x/commerce</code> (where <code class="highlighter-rouge">USERNAME</code> is your own GitHub username as provided in the form, below) we should see the two subdirectories (<code class="highlighter-rouge">auctions</code>, <code class="highlighter-rouge">commerce</code>) and the <code class="highlighter-rouge">manage.py</code> file. If that’s not how your code is organized when you check, reorganize your repository needed to match this paradigm.</p>
</div>

<ol start="3">
  <li>If you’ve installed <code class="highlighter-rouge">submit50</code>, execute
    <div class="highlighter-rouge"><div class="highlight"><pre class="highlight"><code>submit50 web50/projects/2020/x/commerce
</code></pre></div>    </div>
    <p>Otherwise, using Git, push your work to <code class="highlighter-rouge">https://github.com/me50/USERNAME.git</code>, where <code class="highlighter-rouge">USERNAME</code> is your GitHub username, on a branch called <code class="highlighter-rouge">web50/projects/2020/x/commerce</code>.</p>
  </li>
  <li><a href="https://www.howtogeek.com/205742/how-to-record-your-windows-mac-linux-android-or-ios-screen/">Record a screencast</a>
 not to exceed 5 minutes in length, in which you demonstrate your 
project’s functionality. Be certain that every element of the 
specification, above, is demonstrated in your video. There’s no need to 
show your code in this video, just your application in action; we’ll 
review your code on GitHub. <a href="https://www.youtube.com/upload">Upload that video to YouTube</a>
 (as unlisted or public, but not private) or somewhere else. In your 
video’s description, you must timestamp where your video demonstrates 
each of the seven (7) elements of the specification. <strong>This is not optional</strong>, videos without timestamps in their description will be automatically rejected.</li>
  <li>Submit <a href="https://forms.cs50.io/b9cc62aa-f3b9-4bce-9360-282ec2cab5b6">this form</a>.</li>
</ol>

<p>You can then go to <a href="https://cs50.me/cs50w">https://cs50.me/cs50w</a> to view your current progress!</p>

