<div class="alert mb-0 shadow fixed-top alert-danger" data-alert="danger" id="alert" role="alert">

This is CS50W 2018, an older version of the course. See [cs50.harvard.edu/web](https://cs50.harvard.edu/web/) for the latest!

</div>

<div class="container-fluid">

<div class="row">

<aside class="col-md" style="height: 1752.5px; top: 50.5px;">

<header>

# [CS50’s Web Programming with Python and JavaScript](https://cs50.harvard.edu/web/2018/)

OpenCourseWare

[Brian Yu](https://brianyu.me/)  
[brian@cs.harvard.edu](mailto:brian@cs.harvard.edu)

[David J. Malan](https://cs.harvard.edu/malan/)  
[malan@harvard.edu](mailto:malan@harvard.edu)  
[](https://www.facebook.com/dmalan)[](https://github.com/dmalan)[](https://www.instagram.com/davidjmalan/)[](https://www.linkedin.com/in/malan/)[](https://www.quora.com/profile/David-J-Malan)[](https://twitter.com/davidjmalan)

</header>

<button aria-controls="nav" aria-expanded="false" class="btn btn-sm collapsed d-md-none" data-target="aside > nav" data-toggle="collapse">Menu</button>

<nav class="collapse d-md-block" id="nav">

* * *

1.  [Git](https://cs50.harvard.edu/web/2018/#git)
2.  [HTML, CSS](https://cs50.harvard.edu/web/2018/#html-css)
3.  [Flask](https://cs50.harvard.edu/web/2018/#flask)
4.  [SQL](https://cs50.harvard.edu/web/2018/#sql)
5.  [ORMs, APIs](https://cs50.harvard.edu/web/2018/#orms-apis)
6.  [JavaScript](https://cs50.harvard.edu/web/2018/#javascript)
7.  [Front Ends](https://cs50.harvard.edu/web/2018/#front-ends)
8.  [Django](https://cs50.harvard.edu/web/2018/#django)
9.  [Testing, CI/CD](https://cs50.harvard.edu/web/2018/#testing-cicd)
10.  [GitHub, Travis CI](https://cs50.harvard.edu/web/2018/#github-travis-ci)
11.  [Scalability](https://cs50.harvard.edu/web/2018/#scalability)
12.  [Security](https://cs50.harvard.edu/web/2018/#security)

* * *

*   <span class="fa-li"></span>[CS50 Certificate](https://cs50.harvard.edu/web/2018/certificate/)
*   <span class="fa-li"></span>[FAQ](https://cs50.harvard.edu/web/2018/faqs/)

* * *

*   <span class="fa-li"></span>[Ed Discussion](https://cs50.harvard.edu/web/ed) for Q&A
*   <span class="fa-li"></span>[Quick Start Guide](https://cs50.harvard.edu/web/2018/quickstart.pdf)

* * *

*   <span class="fa-li"></span>[edX](https://cs50.edx.org/web)
*   <span class="fa-li"></span>[iTunes U](https://itunes.apple.com/us/course/cs50s-web-programming-with-python-and-javascript/id1409824690)
*   <span class="fa-li"></span>[YouTube](https://www.youtube.com/playlist?list=PLhQjrBD2T382hIW-IsOVuXP1uMzEvmcE5)

* * *

*   <span class="fa-li"></span>[Discord](https://discord.gg/T8QZqRx)
*   <span class="fa-li"></span>[Facebook Group](https://www.facebook.com/groups/cs50/)
*   <span class="fa-li"></span>[Facebook Page](https://www.facebook.com/cs50/)
*   <span class="fa-li"></span>[Instagram](https://instagram.com/cs50)
*   <span class="fa-li"></span>[LinkedIn Group](https://www.linkedin.com/groups/7437240/)
*   <span class="fa-li"></span>[Quora](https://www.quora.com/topic/CS50)
*   <span class="fa-li"></span>[Snapchat](https://www.snapchat.com/add/cs50)
*   <span class="fa-li"></span>[Twitter](https://twitter.com/cs50)
*   <span class="fa-li"></span>[YouTube](http://www.youtube.com/subscription_center?add_user=cs50tv)

* * *

[![Harvard Shop](img/uc.png)](https://cs50.harvardshop.com/)

* * *

</nav>




</aside>